import math
def answer(l):
    if(len(l) == 1 and l[0]%3 != 0):
        return int(0)
    if(sum(l)%3 == 0):
        l.sort(reverse = True)
        return convert(l)
    else:
        hash = {}
        l.sort()
        for i in l:
            hash[i] = sum(l) - i
        for i in hash:
            if(hash[i]%3 == 0):
                l.remove(i);
                l.sort(reverse = True)
                return convert(l)
        l.sort()
        l.remove(l[0])
        return answer(l)

def convert(k):
    num = 0
    for i in range(0,len(k)):
        num = num+(k[i]*(math.pow(10,len(k)-1 - i)))
    return int(num)


print answer([1,0,2,4,6,8])