class Node:
  def __init__(self,data,next):
    self.data = data
    self.next = next
class List:
  def __init__(self):
    self.root = Node(0,None)
    
    
  def add(self,data):
    root = self.root
    while(root.next != None):
      root = root.next
    root.next = Node(data,None)
    
  def _print(self):
    root = self.root
    while(root.next != None):
      print(root.data)
      root = root.next
    
li = List()
li.add(10)
li.add(20)
li.add(30)
li.add(40)
li._print()