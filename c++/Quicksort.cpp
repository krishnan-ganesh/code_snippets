#include <iostream>

using namespace std;
int partition(int * arr,int low,int high)
{
  int pointer = high - 1;
  int index = low;
   for (int i = low;i<high;i++)
   {
     if(arr[i] < arr[pointer])
     {
       int temp = arr[index];
       arr[index] = arr[i];
       arr[i] = temp;
       index++;
     }
   }
   int temp = arr[pointer];
       arr[pointer] = arr[index];
       arr[index] = temp;
       return index;
}

void quicksort(int* arr,int low,int high)
{
    if(low<high)
    {
        int point = partition(arr,low,high);
        quicksort(arr,low,point);
        quicksort(arr,point+1,high);
    }
}



int main()
{
 int arr[] = {5,88,7,65,28,28,91,469};
 quicksort(arr,0,8);
 int i = 0;
    while(i<8)
 {
  cout<<arr[i]<<endl;
  i++;
 }
}

