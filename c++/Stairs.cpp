#include <iostream>
using namespace std;
void Staircase(int n){
  for(int i = 0;i<n;++i){
    for(int j = n;j>i+1;--j){
      cout<<' ';
    }
    for(int k = 0;k<i+1;++k){
      cout<<'*';
    }
    
    cout<<"\n";
  }
}

void IStaircase(int n){
  for(int i = 1;i<n+1;++i){
    for(int j = 0;j<i;++j){
      cout<<'*';
    }
    cout<<"\n";
  }
}
int main() {
    Staircase(5);
    IStaircase(5);
}