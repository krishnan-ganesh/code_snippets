//Time Complexity: O(n*n!) Note that there are n! permutations and it requires O(n) time to print a a permutation.

#include <iostream>
#include <cstring>

using namespace std;


void swap(char*,char*);
void Permute(char*,int,int);

int main()
{
   cout << "Hello World" << endl; 
   char s[] = "ABCD";
   int n = strlen(s);
   Permute(s,0,n-1);
   return 0;
}
void swap(char *x, char *y)
{
    char temp;
    temp = *x;
    *x = *y;
    *y = temp;
}

void Permute(char* s,int l,int r)
{
    if(l == r)
    {
        cout<<s<<endl;
    }
    else
    {
        for(int i = l;i<= r;i++)
        {
            swap((s+l),(s+i));
            Permute(s,l+1,r);
            swap((s+l),(s+i));
        }
    }
}

