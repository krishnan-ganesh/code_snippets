import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        Node tri = new Node();
        
        for(int a0 = 0; a0 < n; a0++){
            String op = in.next();
            String contact = in.next();
            
            if(op.equals("add")){
                tri.addString(contact,0);
            }
            if(op.equals("find")){
               System.out.println(tri.findCount(contact,0,tri));
            }
        }
    }
}

class Node{
	Node[] arr = new Node[26];
	public int size;
	
	public void addString(String s,int index){
		size++;
		if(index == s.length())return;
		char current = s.charAt(index);
		int nodeIndex = current - 'a';
		
		if(arr[nodeIndex] == null){
			Node child = new Node();
			arr[nodeIndex] = child;
		}
		
		
		arr[nodeIndex].addString(s,index+1);
	}
	
	public int findCount(String s,int index,Node n){
		if(index == s.length())
			return n.size;
		if(n.arr[s.charAt(index) - 'a'] == null){
			return 0;
		}
		return findCount(s,index+1,n.arr[s.charAt(index) - 'a']);
		 
	}
}