import java.util.*;
import java.util.Collections;

class Main {
  public static void main(String[] args) {
    PriorityQueue<Integer> pq = new PriorityQueue<Integer>(new Comparator<Integer>(){
    	public int compare(Integer lhs,Integer rhs){
    		if (lhs > rhs) return -1;
        if (lhs.equals(rhs)) return 0;
        return 1;
    	}
    });
    pq.add(10);
    pq.add(20);
    pq.add(30);
    pq.add(40);
   
    System.out.println(pq.poll());
  }
}