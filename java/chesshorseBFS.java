https://repl.it/Lbie/4

import java.util.*;
class grid{
 int X;
 int Y;
 grid previous;
   grid(int x,int y,grid previous){
    this.X = x;
    this.Y = y;
    this.previous = previous;
  }
}

class Main{
  public static void main(String args[]){
    grid start = new grid(2,2,null);
    
    grid end = new grid(3,3,null);
    
    new BFS().runBFS(start,end);
  }
}

class BFS{
  Queue q = new LinkedList();
  public void runBFS(grid start,grid end){
    helper(start,end);
  }
  
  public void helper(grid start,grid end){
    if(!compareGrid(start,end)){
      addToQueue(start);
      if(q.peek() != null){
        grid holder = (grid)q.poll();
        helper(holder,end);
      }
    }
    else{
      while(start.previous != null){
        System.out.println(start.X + "<->" + start.Y);
        start = start.previous;
      }
      System.out.println(start.X + "<->" + start.Y);
    }
  }
  
  public void addToQueue(grid start){
    int a =start.X;
    int b = start.Y;
    if(validSquare(a+2,b+1)){
      q.add(new grid(a+2,b+1,start));
    }
    
    if(validSquare(a+2,b-1)){
      q.add(new grid(a+2,b-1,start));
    }
    
    if(validSquare(a-2,b-1)){
      q.add(new grid(a-2,b-1,start));
    }
    
    if(validSquare(a-2,b+1)){
      q.add(new grid(a-2,b+1,start));
    }
    
    if(validSquare(a+1,b+2)){
      q.add(new grid(a+1,b+2,start));
    }
    
    if(validSquare(a+1,b-2)){
      q.add(new grid(a+1,b-2,start));
    }
    
    if(validSquare(a-1,b+2)){
      q.add(new grid(a-1,b+2,start));
    }
    
    if(validSquare(a-1,b-2)){
      q.add(new grid(a-1,b-2,start));
    }
  }
  
  public boolean validSquare(int x,int y){
    if(x>=1 && x<=8 && y>=1 && y<=8){
      return true;
    }
    return false;
  }
  
  public boolean compareGrid(grid a,grid b){
    if(a.X == b.X && a.Y == b.Y){
      return true;
    }
    return false;
  }
}