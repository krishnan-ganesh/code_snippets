// https://repl.it/EQZg

import java.util.*;
import java.util.Collections;

class Main {
  public static void main(String[] args) {
   PriorityQueue<Integer> max_heap = new PriorityQueue<Integer>(new Comparator<Integer>(){
    	public int compare(Integer lhs,Integer rhs){
    		if (lhs > rhs) return -1;
        if (lhs.equals(rhs)) return 0;
        return 1;
    	}
    });
    PriorityQueue<Integer> min_heap = new PriorityQueue<Integer>();
    
    Scanner input = new Scanner(System.in);
    int i = 0;
    while(i<20){
    	int num = input.nextInt();
    	add(min_heap,max_heap,num);
    	System.out.println(getMedian(min_heap,max_heap));
    	i++;
    }
    
    
  }
   public static void add(PriorityQueue<Integer> min,PriorityQueue<Integer> max,Integer num){
  	min.add(num);
  	rebalance(min,max);
  }
  
  public static void rebalance(PriorityQueue<Integer> min,PriorityQueue<Integer> max){
  	if(min.size() - max.size() > 1){
  		max.add(min.poll());
  	}
  }
  
  public static double getMedian(PriorityQueue<Integer> min,PriorityQueue<Integer> max){
  	
  	if(min.size() == max.size()){
  		return (double)(min.peek()+max.peek())/2;
  	}
  	return min.peek();
  	
  }
  
}
