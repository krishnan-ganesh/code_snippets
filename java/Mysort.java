class Main {
  public static void main(String[] args) {
    int[] a = {1,9,9,6,3,2,5};
    int[] result = mysort(a);
    for(int i= 0;i<a.length;i++){
      System.out.println(result[i]);
    }
  }
  
  public static int[] mysort(int[] a){
    int[] result = new int[a.length];
    for(int i= 0;i<a.length;i++){
      int index = 0;
      for(int j = 0;j<a.length;j++){
        if(a[i]>a[j]){
          index++;
        }
      }
      result[index] = a[i];
    }
    return result;
  }
}