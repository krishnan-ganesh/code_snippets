class Main {
  public static void main(String[] args) {
    Node l = new Node(0);
    l.add(10);
    l.add(20);
    l.add(30);
    l.add(40);
    l.add(50);
    l.print();
    Node root = LL_reverser.reverser(l);
    LL_reverser.print(root);
  }
}
class Node{
  public int data;
  Node next;
  public Node(int num){
    data = num;
    next = null;
  }
  public void add(int num){
    Node holder = this;
    while(holder.next!=null){
      holder = holder.next;
    }
    Node temp = new Node(num);
    holder.next = temp;
  }
  
  public void print(){
    Node temp = this;
    while(temp.next !=null){
      System.out.println(temp.data);
      temp = temp.next;
    }
    System.out.println(temp.data);
  }
}
class LL_reverser{
  public static Node reverser(Node traveller){
    Node holder = traveller;
    Node Previous = null;
    while(holder != null){
    Node next = holder.next;
    holder.next = Previous;
    Previous = holder;
    holder = next;
    }
    return Previous;
  }
  
  public static void print(Node root){
    while(root.next != null){
      System.out.println(root.data);
      root = root.next;
    }
  }
}