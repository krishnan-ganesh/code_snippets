import java.util.*;

import java.lang.*;

class Roman_Numerals {
  public static void main(String[] args) {
  	HashMap<Integer,String> hm = new HashMap<Integer,String>();
  	fillMap(hm);
  	int givenNumber = 889;
  	
  	System.out.println(romNum(givenNumber,hm));
    
  }
  
  public static String romNum(int number,HashMap hm){
  	String res = "";
  	String num = String.valueOf(number);
  	for(int i = num.length() ; i>0 ; i--){
  		int place = Integer.parseInt(num.substring(i-1,i))*((int)Math.pow(10,num.length() - i));
  		res = (String)hm.get(place) + res;
  		
  	}
  	return res;
  }
  
  public static void fillMap(HashMap hm){
  	hm.put(0,"");
  	hm.put(1,"I");
	hm.put(2,"II");
	hm.put(3,"III");
  	hm.put(4,"IV");
  	hm.put(5,"v");
	hm.put(6,"VI");
	hm.put(7,"VII");
	hm.put(8,"VIII");
  	hm.put(9,"IX");
  	hm.put(10,"X");
	hm.put(20,"XX");
	hm.put(30,"XXX");
  	hm.put(40,"XL");
  	hm.put(50,"L");
  	hm.put(60,"LX");
  	hm.put(70,"LXX");
  	hm.put(80,"LXXX");
  	hm.put(90,"XC");
  	hm.put(100,"C");
	hm.put(200,"CC");
	hm.put(300,"CCC");
  	hm.put(400,"CD");
  	hm.put(500,"D");
  	hm.put(600,"DC");
  	hm.put(700,"DCC");
  	hm.put(800,"DCCC");
  	hm.put(900,"CM");
  	hm.put(1000,"M");
  
  	
  }
}
