import java.util.*;

public class Solution {

    
    public static int fibonacci(int n) {
        // Complete the function.
      int[] result = new int[n+1];
        result[0] = 0;
        result[1] = 1;
        for(int i = 2;i<=n;i++){
            result[i] = result[i-2]+result[i-1];
            
        }
        return result[n];
      
    }
    

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        scanner.close();
        System.out.println(fibonacci(n));
    }
}
