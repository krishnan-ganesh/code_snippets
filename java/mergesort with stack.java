import java.util.*;
import java.util.Stack;
import java.util.Arrays;
class Main {
  public static void main(String[] args) {
    System.out.println("hello world");
    int[] arr = {8,6,1,2,3,4,7,5};
    System.out.println(Arrays.toString(M.mergesort(arr)));
  }
}

class M{
  public static int[] mergesort(int[] arr){
    return split(arr);
  }
  
  public static int[] split(int[] arr){
    int len = arr.length;
    int[] leftarr = Arrays.copyOfRange(arr, 0, len/2);
    int[] rightarr = Arrays.copyOfRange(arr, len/2, len);
    if(arr.length>2){
    leftarr = split(leftarr);
    rightarr = split(rightarr);
    }
    
    return merge(leftarr,rightarr);
  } 
  
  public static int[] merge(int[] left,int[] right){
    Stack lefts = new Stack();
    Stack rights = new Stack();
    int[] result = new int[left.length+right.length];
    int j = 0;
    
    for(int i = left.length - 1;i>-1;i--){
      lefts.push(left[i]);
    }
    for(int i = right.length - 1;i >-1;i--){
      rights.push(right[i]);
    }
    
    while(!(lefts.empty() || rights.empty())){
      if((Integer)lefts.peek() > (Integer)rights.peek()){
        result[j] = (Integer)lefts.pop();
      }
      else{
        result[j] = (Integer)rights.pop();
      }
      j++;
    }
    if(lefts.empty()){
      while(!rights.empty()){
        result[j] = (Integer)rights.pop();
        j++;
      }
    }
    
    if(rights.empty()){
      while(!lefts.empty()){
        result[j] = (Integer)lefts.pop();
        j++;
      }
    }
    
    return result;
  }
}