import java.util.*;

class Main
{
  public static void main(String[] args)
  {
    HashMap<Integer,Long> hash = new HashMap<Integer,Long>();
    System.out.println(factorial(20,hash));
  }

  static Long factorial(int number,HashMap<Integer,Long> hash)
  {

    if(number == 0)
    {
      return new Long(1);
    }
    else if(hash.containsKey(number))
    {
      return hash.get(number);
    }
    else
    {
      hash.put(number,number*factorial(number - 1,hash));
      return hash.get(number);
    }
  }
}
