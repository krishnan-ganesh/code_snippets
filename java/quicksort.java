
import java.util.Arrays;
class Main {
  public static void main(String[] args) {
    int[] arr = {2,3,8,99,7,6,4,5,88};
    Q.quicksort(arr,0,8);
  }
}

class Q{
  public static void quicksort(int[] arr,int low,int high){
    System.out.println(low +" "+ high);
    if(low<high){
      int point = partition(arr,low,high);
     // System.out.println(point);
      quicksort(arr,low,point);
      quicksort(arr,point+1,high);
    }
  }
  
    public static int partition(int[] arr,int low,int high){
      int index = low;
      for(int i = low;i<high;i++){
        if(arr[high-1] > arr[i])
        {
          int temp = arr[index];
          arr[index] = arr[i];
          arr[i] = temp;
          System.out.println(index);
          index++;
        }
      }
      int temp = arr[index];
          arr[index] = arr[high-1];
          arr[high-1] = temp;
          System.out.println(Arrays.toString(arr));
      return index;
    }
}