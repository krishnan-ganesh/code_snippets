using System.IO;
using System;
using System.Collections;

class Program
{
    static void Main()
    {
        int[] arr = {1,1,2,2,2,2,2,3,3,3,3,3,3,3,3,5,6,4,4,4,8,9,7,5,22};
        Hashtable ht = new Hashtable();
        foreach(int i in  arr)
        {
            if(!ht.ContainsKey(i))
            {
                ht.Add(i,1);
            }
            else
            {
                int count = (int)ht[i];
                ht.Remove(i);
                ht.Add(i,count+1);
            }
            
            
        }
        foreach (DictionaryEntry entry in ht)
	       {
	    Console.WriteLine("{0}, {1}", entry.Key, entry.Value);
	       }
    }
}