using System;
using System.Collection.Generic;

namespace Graph
{
  class Main
    {
       public static void Main()
       {
         Node a = new Node();
         Node b = new Node();
         Node c = new Node();

         a.data = 4;
         a.ls.add(b,c);
         b.data = 5;
         c.data = 6;
       }
    }
  class Node
  {
    int data;
    List<Node> ls;
  }
}