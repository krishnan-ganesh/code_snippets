using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace NonRepeating
{
	class Program
	{
		public static void Main()
		{
			string name = "karthik";
			Program obj = new Program();
			char[] char_array = name.ToCharArray();
			char_array.Reverse();
			
			int[] result = obj.last_nr_character(name);
			
			
			foreach (char i in char_array)
            {
            	if(result[i] == 1)
            	{
            		Console.WriteLine(i);
            		break;
            	}
            }
		}
		public int[] last_nr_character(string name)
		{
            char[] char_array = name.ToCharArray();
			char_array.Reverse();
            int[] Dummy = new int[256];
            foreach (char i in char_array)
            {
            	Dummy[i]++;
            }
            return Dummy;        
		}
	}
}