using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
class Solution {

    static void Main(String[] args) {
        string a = Console.ReadLine();
        string b = Console.ReadLine();
        char[] a_c = a.ToCharArray();
        char[] b_c = b.ToCharArray();
        Hashtable ht = new Hashtable();
        Hashtable ht2 = new Hashtable();
        int main_count = 0;
        
        
        foreach(char i in a_c )
        {
            if(!ht.ContainsKey(i))
            {
                ht.Add(i,1);
            }
            else
            {
                int count = (int)ht[i];
                ht.Remove(i);
                ht.Add(i,count+1);
            }
        }
        foreach(char j in b_c )
        {
            if(!ht2.ContainsKey(j))
            {
                ht2.Add(j,1);
            }
            else
            {
                int count =(int)ht2[j];
                ht2.Remove(j);
                ht2.Add(j,count+1);
            }
        }
       
        
        foreach(DictionaryEntry entry in ht2)
        {
            if(ht.ContainsKey(entry.Key))
            {
                int count = Math.Abs((int)entry.Value - (int)ht[entry.Key]);
                ht.Remove(entry.Key);
                ht.Add(entry.Key,count);
            }
            else
            {
                int count = (int)entry.Value;
                ht.Add(entry.Key,count);
            }
        }
        foreach (DictionaryEntry entry in ht)
	       {
	           main_count += Math.Abs((int)entry.Value);
	       }
        
        Console.WriteLine(main_count);
    }
}